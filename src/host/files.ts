/// <reference types="node" />

// The MIT License (MIT)
// 
// vs-remote-editor (https://github.com/mkloubert/vs-remote-editor)
// Copyright (c) Marcel Joachim Kloubert <marcel.kloubert@gmx.net>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

import * as FS from 'fs';
import * as i18 from '../i18';
import * as Path from 'path';
import * as re_helpers from '../helpers';
import * as re_host from '../host';
import * as re_host_helpers from './helpers';
import * as re_users from '../host/users';
import * as URL from 'url';


/**
 * Handles a local file.
 * 
 * @param {string} file The local file.
 * @param {re_host.RequestContext} ctx The request context.
 */
export function handleFile(file: string,
                           ctx: re_host.RequestContext) {
    ctx.user.isFileVisible(file).then((isVisible) => {
        if (!isVisible) {
            re_host_helpers.sendNotFound(ctx);
            return;
        }

        //TODO
        ctx.response.end();
    }).catch((err) => {
        re_host_helpers.sendError(err, ctx);
    });
}
