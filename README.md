# vs-remote-editor

A [Visual Studio Code](https://code.visualstudio.com/) (VS Code) extension running a HTTP server, which makes it possible to work with the current workspace remotely.

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=74YKCZZXPCP64) [![](https://api.flattr.com/button/flattr-badge-large.png)](https://flattr.com/submit/auto?fid=o62pkd&url=https%3A%2F%2Fgithub.com%2Fmkloubert%2Fvs-remote-editor)
